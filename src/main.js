// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import VueRouter from 'vue-router';
Vue.use(VueRouter);


import VueResource from 'vue-resource';
Vue.use(VueResource);

//引入vant
import Vant from 'vant';
import 'vant/lib/index.css';
Vue.use(Vant);

//引入适配rem.js
import '../rem.js';
//引入全局样式
import '../pubilc.css';
//引入mui
import './assets/mui/css/mui.min.css'
//引入vuex
import Vuex from 'vuex'
//注册vuex到vue中
Vue.use(Vuex)

var cart = JSON.parse(localStorage.getItem('cart') || '[]')
    //new Vuex.Store()实例 得到一个数据仓储对象
var store = new Vuex.Store({
    state: {
        cart: cart
    },
    mutations: {
        addToCart(state, goodsinfo) {
            //点击购物车 把商品信息存到store的cart
            var flag = false
            state.cart.some(item => {
                if (item.id == goodsinfo.id) {
                    item.count += goodsinfo.count
                        //console.log(item.count)
                    flag = true;
                    return true
                }
            })
            if (flag === false) {
                state.cart.push(goodsinfo)
            }
            //当更新cart  把数据存储到本地的localStorage中
            localStorage.setItem('cart', JSON.stringify(state.cart))

        },
        updatedGoodCount(state, goodslist) {
            state.cart.some(item => {
                if (item.id == goodslist.id) {
                    item.count = parseInt(goodslist.count)
                }
            })
            localStorage.setItem('cart', JSON.stringify(state.cart))
                /* console.log(state.cart) */

        },
        removeFormCart(state, id) {
            //根据id从store中的购物车删除对应的那条商品数据
            state.cart.some((item, i) => {
                if (item.id == id) {
                    state.cart.splice(i, 1)
                    return true;
                }
            })
            localStorage.setItem('cart', JSON.stringify(state.cart))
        },
        updateGoodsSelected(state, info) {
            state.cart.some(item => {
                if (item.id == info.id) {
                    item.selected = info.selected;
                } else {
                    item.selected = false;
                }
            })
            localStorage.setItem('cart', JSON.stringify(state.cart))
                //console.log(state.cart)
        }
    },
    getters: {
        getAllCount(state) {
            var c = 0;
            state.cart.forEach(item => {
                c += item.count
            })
            return c
        },
        getGoodsSelected(state) {
            var o = []
            state.cart.forEach(item => {
                if (item.selected) {
                    o.push(item.id)
                }
            })
            return o
        },
        getGoodsCountAndAmount(state) {
            var o = {
                count: 0,
                amount: 0
            }
            state.cart.forEach(item => {
                if (item.selected) {
                    o.count += item.count;
                    o.amount += item.price * item.count
                }
                return o
            })
        }
    }
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>',
    store //将vuex创建的实例挂载到vm实例上
})