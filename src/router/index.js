import Vue from 'vue'
import Router from 'vue-router'
import index from '@/components/index'
import plan from '@/components/plan'
import goodlist from '@/components/goodlist'
import goodListDetail from '@/components/goodListDetail'
import good from '@/components/good'
import cart from '@/components/cart'

Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            redirect: '/index'
        },
        {
            path: '/index',
            name: 'index',
            component: index
        },
        {
            path: '/plan',
            name: 'plan',
            component: plan
        },
        {
            path: '/goodlist',
            name: 'goodlist',
            component: goodlist
        },
        {
            path: '/goodlist/goodListDetail',
            name: 'goodListDetail',
            component: goodListDetail
        },
        {
            path: '/goodlist/goodListDetail/good',
            name: 'good',
            component: good
        },
        {
            path: '/cart',
            name: 'cart',
            component: cart
        }

    ],
    linkActiveClass: 'mui-active'
})