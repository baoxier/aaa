var html = document.getElementsByTagName('html')[0];
var width = window.innerWidth;
var fontSize = 100 / 375 * width;

html.style.fontSize = fontSize + 'px';
window.onresize = function() {
    var html = document.getElementsByTagName('html')[0];
    var width = window.innerWidth;
    var fontSize = 100 / 375 * width;
    html.style.fontSize = fontSize + 'px';
}